# Python installieren

Installiere zuallererst Python, optimalerweise die neuste Stable-Version (3.x), auf deinem Computer.
Um zu prüfen, ob Python bereits installiert ist:

```
python --version
```

# Python-Pip und -Venv installieren

Installiere zusätzlich diese beiden Pakete:

```
sudo apt install python3-pip
```

```
sudo apt install python3-venv
```

# Virtuelle Programmierumgebung erstellen und aktivieren

Stelle sicher, dass du dich im Hauptverzeichnis dieses Projekts befindest.
Dann führe diesen Befehl aus, um ein virtuelles Python-Environment zu erstellen:

```
python3 -m venv .venv
```

Aktiviere die erstellte Umgebung:

```
source .venv/bin/activate
```

# Abhängigkeiten installieren

Führe dafür diesen Befehl aus, während du dich in diesem Ordner befindest:

```
pip3 install -r requirements.txt
```

# Dateien vorbereiten

Lege das Coverfoto ("cover.jpg") und alle Fotos im Format "\<Monat-Nr\>\<Monat-Name\>.jpg" (z. B. "1Januar.jpg") im Ordner "photos" ab.

Achte dabei darauf, dass das Seitenverhältnis 4961 x 2923 beträgt, oder passe ggf. den Code an.

Bearbeite die Dateien "descriptions.json" und "accent_colors.json", falls nötig.

# Ausführen

Führe das Skript aus:

```
python3 main.py
```
