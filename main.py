import locale
import calendar
import datetime
import json
from PIL import Image, ImageFont, ImageDraw, ImageCms

year = 2021
name = 'Torben Wetter'
file_ext = 'jpeg'

# DIN A3
width, height = 4961, 3508

# manually calculated/set values
photo_height = height * 5 // 6  # 2923
footer_height = height - photo_height  # 585
cover_title_dist = footer_height // 8  # 73
cover_subtitle_dist = footer_height // 2  # 292
description_dist = footer_height // 29  # 20
description_margin = width // 65  # 76
month_name_dist = footer_height // 10  # 58
day_row_dist = footer_height // 2  # 292
digit_margin = width // 950  # 5
day_margin = width // 220  # 22
week_offset = footer_height // 8  # 73
day_name_offset = footer_height // 6  # 97

# locale for month name, etc.
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

cal = calendar.Calendar()

# colors in RGB format
white = '#FFFFFF'
light_grey = '#AFAFAF'

# set fonts
cover_title_font = ImageFont.truetype("CourierNewBold.ttf", size=width // 21)  # 236
cover_subtitle_font = ImageFont.truetype("CourierNew.ttf", size=width // 28)  # 177
description_font = ImageFont.truetype("CourierNewBold.ttf", size=width // 78)  # 63
month_name_font = ImageFont.truetype("CourierNewBold.ttf", size=width // 42)  # 118
day_font = ImageFont.truetype("CourierNewBold.ttf", size=width // 43)  # 115
week_font = ImageFont.truetype("CourierNewBold.ttf", size=width // 65)  # 76
day_name_font = ImageFont.truetype("CourierNewBold.ttf", size=width // 62)  # 80

# load description texts
photo_descriptions = json.load(open('descriptions.json', 'r'))

# load accent colors
accent_colors = json.load(open('accent_colors.json', 'r'))

# convert from RGB to CMYK color profile
def convert_to_cmyk(image):
    return ImageCms.profileToProfile(image, 'sRGB_v4_ICC_preference.icc', 'ISOcoated_v2_300_eci.icc', renderingIntent=0, outputMode='CMYK')

# # # COVER PAGE # # #

# create black image with correct size
image = Image.new('RGB', (width, height), 'black')

# load and paste photo (must have the right aspect ratio already)
photo = Image.open('photos/cover.' + file_ext).resize((width, photo_height))
image.paste(photo, (0, 0))

# create a draw-Object (for text, etc.)
draw = ImageDraw.Draw(image)

# calculate text width to center text
cover_title_text = str(year)
cover_title_w, _ = draw.textsize(cover_title_text, font=cover_title_font)
cover_title_x = (width - cover_title_w) // 2
cover_title_y = photo_height + cover_title_dist
draw.text((cover_title_x, cover_title_y), cover_title_text, fill=white, font=cover_title_font)

# calculate text width to center text
cover_subtitle_text = name
cover_subtitle_w, _ = draw.textsize(cover_subtitle_text, font=cover_subtitle_font)
cover_subtitle_x = (width - cover_subtitle_w) // 2
cover_subtitle_y = photo_height + cover_subtitle_dist
draw.text((cover_subtitle_x, cover_subtitle_y), cover_subtitle_text, fill=white, font=cover_subtitle_font)

cover_image = convert_to_cmyk(image)

# # # MONTH PAGES # # #

# loop through months
month_images = []
for month_i in range(1, 13):
    # get month name
    month_name = calendar.month_name[month_i]

    # create black image with correct size
    image = Image.new('RGB', (width, height), 'black')

    # load and paste photo (must have the right aspect ratio already)
    photo = Image.open('photos/{}{}.{}'.format(month_i, month_name, file_ext)).resize((width, photo_height))
    image.paste(photo, (0, 0))

    # create a draw-Object (for text, etc.)
    draw = ImageDraw.Draw(image)

    photo_description = photo_descriptions[month_i - 1]
    # calculate text width to align text right
    description_text_w, _ = draw.textsize(photo_description, font=description_font)
    description_x = width - description_text_w - description_margin
    description_y = photo_height + description_dist
    draw.text((description_x, description_y), photo_description, fill=white, font=description_font)

    # calculate text width to center text
    month_text_w, _ = draw.textsize(month_name, font=month_name_font)
    month_name_x = (width - month_text_w) // 2
    month_name_y = photo_height + month_name_dist
    draw.text((month_name_x, month_name_y), month_name, fill=white, font=month_name_font)

    accent_color = accent_colors[month_i - 1]

    days = [(day, week_day) for day, week_day in cal.itermonthdays2(year, month_i) if day != 0]

    digit_w, _ = draw.textsize('0', font=day_font)
    digit_offset = digit_w - digit_margin
    day_width = 2 * digit_offset + day_margin
    day_row_margin = (width - len(days) * day_width) / 2

    # loop through days (and week day names) in month
    for day, week_day in days:
        day_number_s = ('0' if day < 10 else '') + str(day)
        digit1, digit2 = day_number_s

        day_color = accent_color if week_day == 5 or week_day == 6 else white

        day_x = day_row_margin + (day - 1) * day_width
        day_y = photo_height + day_row_dist
        # draw digits separately to have control over letter spacing
        draw.text((day_x, day_y), digit1, fill=day_color, font=day_font)
        draw.text((day_x + digit_offset, day_y), digit2, fill=day_color, font=day_font)

        # draw week number (of the year)
        if week_day == 0:
            week_number = datetime.date(year, month_i, day).isocalendar()[1]
            week_name = str(week_number) + '›'
            week_y = day_y - week_offset
            draw.text((day_x, week_y), week_name, fill=light_grey, font=week_font)

        day_name = calendar.day_name[week_day][:2].lower()
        day_name_color = accent_color if week_day == 5 or week_day == 6 else light_grey
        day_name_w, _ = draw.textsize(day_name, font=day_name_font)
        day_name_margin = (day_width - day_name_w) / 2 - digit_margin
        day_name_x = day_x + day_name_margin
        day_name_y = day_y + day_name_offset
        draw.text((day_name_x, day_name_y), day_name, fill=day_name_color, font=day_name_font)

    # add month image to image list
    month_images.append(convert_to_cmyk(image))

# export all images to one PDF file
cover_image.save('calendar.pdf', save_all=True, append_images=month_images)
